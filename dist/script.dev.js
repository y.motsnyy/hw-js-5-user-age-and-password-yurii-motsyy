"use strict";

var createNewUser = function createNewUser() {
  var firstName = prompt('Enter new user\'s first name');
  var lastName = prompt('Enter new user\'s last name');
  var birthDate = prompt('Enter new user\'s date of birth', 'dd.mm.yyyy');
  var newUser = {
    firstName: firstName,
    lastName: lastName,
    birthDate: birthDate,
    getAge: function getAge() {
      var birthDay = birthDate.slice(0, 2);
      var birtMonth = birthDate.slice(3, 5);
      var birthYear = birthDate.slice(-4, 4);
      var birthDateRightFormat = "".concat(birthYear, "-").concat(birtMonth, "-").concat(birthDay);
      var birthDateMs = Date.parse(birthDateRightFormat);
      var currntDateMs = Date.parse(new Date());
      var ageMs;
      var ageYear;

      if (birthDateMs >= 0) {
        ageMs = currntDateMs - birthDateMs;
      } else {
        ageMs = currntDateMs + birthDateMs * -1;
      }

      ;
      ageYear = Math.trunc(ageMs / 1000 / 3600 / 24 / 365);
      return ageYear;
    },
    getLogin: function getLogin() {
      var login = (this.firstName.charAt(0) + this.lastName).toLowerCase();
      return login;
    },
    getPassword: function getPassword() {
      var password = this.firstName[0] + this.lastName.toLowerCase();
      return password + this.birthDate.substr(-4, 4);
    }
  };
  return newUser;
};

var newUser1 = createNewUser();
console.log(newUser1);
var ageYear1 = newUser1.getAge();
console.log(ageYear1);
var password1 = newUser1.getPassword();
console.log(password1);
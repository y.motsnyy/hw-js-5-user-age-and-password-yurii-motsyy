const createNewUser = function() {
  const firstName = prompt('Enter new user\'s first name');
  const lastName = prompt('Enter new user\'s last name');
  const birthDate = prompt('Enter new user\'s date of birth', 'dd.mm.yyyy');
  
  const newUser = {
    firstName,
    lastName,
    birthDate,
    
    getAge: function() {
      const birthDay = birthDate.slice(0, 2);
      const birtMonth = birthDate.slice(3, 5);
      const birthYear = birthDate.slice(-4, 4);
      const birthDateRightFormat = `${birthYear}-${birtMonth}-${birthDay}`;
      const birthDateMs = Date.parse(birthDateRightFormat);
      const currntDateMs = Date.parse(new Date());
      
      let ageMs;
      let ageYear;
       if (birthDateMs >= 0) {
        ageMs = currntDateMs - birthDateMs;
      } else {
        ageMs = currntDateMs + birthDateMs * -1;
      };
      ageYear = Math.trunc(ageMs/1000/3600/24/365);
      return ageYear;
    },
    
    getLogin: function() {
      const login = (this.firstName.charAt(0) + this.lastName).toLowerCase();
      return login;
    },

    getPassword: function() {
      const password = this.firstName[0] + this.lastName.toLowerCase();
      return password + this.birthDate.substr(-4, 4);
    },
  };
    return newUser;
};

const newUser1 = createNewUser();
console.log(newUser1);

const ageYear1 = newUser1.getAge();
console.log(ageYear1);

const password1 = newUser1.getPassword();
console.log(password1);





  